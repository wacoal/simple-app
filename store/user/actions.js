export default {
  queryUser({ commit }, payload) {
    this.$axios.get('/user', { params: payload }).then((resp) => {
      commit('setUsers', resp.data)
    })
  },

  createUser({ commit }, payload) {
    this.$axios.post('/user', payload).then((resp) => {
      console.log(resp)
    })
  },

  deleteUser({ commit }, payload) {
    this.$axios.delete('/user/' + payload).then((resp) => {
      console.log(resp)
    })
  },

  async getUserById({ commit }, payload) {
    // await this.$axios.get('/user/' + payload).then((resp) => {
    //   commit('setEditId', payload)
    //   commit('setEditUser', resp.data)
    // })
    const resp = await this.$axios.get('/user/' + payload)
    commit('setEditId', payload)
    commit('setEditUser', resp.data)
  },

  updateUser(state, payload) {
    this.$axios.put('/user/' + payload.id, payload.body).then((resp) => {
      console.log(resp)
    })
  },

  activeUser(state, payload) {
    const { id, active } = payload
    this.$axios.put(`/user/active/${id}/${active}`).then((resp) => {
      console.log(resp)
    })
  },
}

FROM node:14-alpine
ENV BASE_URL=http://localhost:3030
ENV APOLLO_URL=http://localhost:3030/graphql
WORKDIR /app
COPY . .
RUN npm i
# universal build
RUN npm run build
EXPOSE 3000
CMD ["npm", "start"]

# docker build -t simple-app .
# docker run --name simple-app -p 3000:3000 simple-app